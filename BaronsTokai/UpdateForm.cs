﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaronsTokai
{
    public partial class UpdateForm : Form
    {
        SqlConnection con = new SqlConnection("Data Source=IBUYAS\\KHANAIK;Initial Catalog=BaronsTokai;Integrated Security=True;");
        SqlCommand cmd;
        SqlDataAdapter adapt;
        int Sno = 0;

        public UpdateForm()
        {
            InitializeComponent();
            DisplayData();
        }

        private void UpdateForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(txtBarcode.Text!="" && txtReg.Text!="" && txtCN.Text!="" && txtLoanTo.Text!="" && txtArd.Text!="" && txtKmri.Text!="")
            {
                cmd = new SqlCommand("update CustomerAgreement set Barcode=@Barcode,RegistrationNo=@RegistrationNo,CustomerName=@CustomerName,LoanDateTo=@LoanDateTo,ActualReturnDate=@ActualReturnDate,KMReadingIn=@KMReadingIn", con);
                con.Open();
                cmd.Parameters.AddWithValue("@SNo", Sno);
                cmd.Parameters.AddWithValue("@Barcode",txtBarcode.Text);
                cmd.Parameters.AddWithValue("@RegistrationNo",txtReg.Text);
                cmd.Parameters.AddWithValue("@CustomerName",txtCN.Text);
                cmd.Parameters.AddWithValue("@LoanDateTo", txtLoanTo.Text);
                cmd.Parameters.AddWithValue("@ActualReturnDate", txtArd.Text);
                cmd.Parameters.AddWithValue("@KMReadingIn",txtKmri.Text);
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Record Updated Successfully");
                DisplayData();
                ClearData();
            }
            else
            {
                MessageBox.Show("Please Select the Record to Update");
            }
        }
        private void DisplayData()
        {
            try
            {
                con.Open();
                DataTable dt = new DataTable();
                adapt = new SqlDataAdapter("Select SNo,Barcode,RegistrationNo,CustomerName,LoanDateTo,ActualReturnDate,KMReadingIn from CustomerAgreement",con);
                adapt.Fill(dt);
                dataGridView1.DataSource = dt;
                con.Close();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        private void ClearData()
        {
            txtBarcode.Text = "";
            txtReg.Text = "";
            txtCN.Text = "";
            txtLoanTo.Text = "";
            txtArd.Text = "";
            txtKmri.Text = "";
            Sno = 0;
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            Sno = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtBarcode.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtReg.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtCN.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtLoanTo.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            txtArd.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtKmri.Text = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();
        }
    }
}
