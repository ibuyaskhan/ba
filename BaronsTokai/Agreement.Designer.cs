﻿namespace BaronsTokai
{
    partial class Agreement_and_Indemnity_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbReferred = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label26 = new System.Windows.Forms.Label();
            this.txtCOVD = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtPolicyno = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtInsurance = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtCDLno = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtCIdNo = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtCBAdd = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtCHAdd = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtReadingin = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtReadingout = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtArd = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtLoandtto = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Loandtfrom = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtVehicle = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtReg = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtEng = new System.Windows.Forms.TextBox();
            this.txtVinn = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Snow;
            this.panel2.Controls.Add(this.txtVinn);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.txtEng);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.linkLabel1);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.cmbReferred);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.dtpDate);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.txtCOVD);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.txtPolicyno);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.txtInsurance);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.txtCDLno);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.txtCIdNo);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.txtEmail);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.txtPhone);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.txtCBAdd);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.txtCHAdd);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.txtName);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.txtReadingin);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.txtReadingout);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.txtArd);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.txtLoandtto);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.Loandtfrom);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.txtVehicle);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.txtReg);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.txtBarcode);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Location = new System.Drawing.Point(12, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1257, 764);
            this.panel2.TabIndex = 0;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.Location = new System.Drawing.Point(923, 677);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(201, 25);
            this.linkLabel1.TabIndex = 46;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Back To Main Screen";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked_1);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(36, 668);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 42);
            this.button1.TabIndex = 45;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbReferred
            // 
            this.cmbReferred.FormattingEnabled = true;
            this.cmbReferred.Items.AddRange(new object[] {
            "Riedwan",
            "Basheer",
            "Ibuyas",
            "Salman"});
            this.cmbReferred.Location = new System.Drawing.Point(929, 505);
            this.cmbReferred.Name = "cmbReferred";
            this.cmbReferred.Size = new System.Drawing.Size(292, 28);
            this.cmbReferred.TabIndex = 44;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(690, 508);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(114, 25);
            this.label27.TabIndex = 41;
            this.label27.Text = "Referred By";
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(305, 501);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(292, 26);
            this.dtpDate.TabIndex = 40;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(31, 501);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 25);
            this.label26.TabIndex = 39;
            this.label26.Text = "Date";
            // 
            // txtCOVD
            // 
            this.txtCOVD.Location = new System.Drawing.Point(929, 453);
            this.txtCOVD.Name = "txtCOVD";
            this.txtCOVD.Size = new System.Drawing.Size(292, 26);
            this.txtCOVD.TabIndex = 38;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(686, 456);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(233, 25);
            this.label25.TabIndex = 37;
            this.label25.Text = "Cust Own Vehicle Details";
            // 
            // txtPolicyno
            // 
            this.txtPolicyno.Location = new System.Drawing.Point(305, 454);
            this.txtPolicyno.Name = "txtPolicyno";
            this.txtPolicyno.Size = new System.Drawing.Size(292, 26);
            this.txtPolicyno.TabIndex = 36;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(31, 452);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(94, 25);
            this.label24.TabIndex = 35;
            this.label24.Text = "Policy No";
            // 
            // txtInsurance
            // 
            this.txtInsurance.Location = new System.Drawing.Point(929, 398);
            this.txtInsurance.Name = "txtInsurance";
            this.txtInsurance.Size = new System.Drawing.Size(292, 26);
            this.txtInsurance.TabIndex = 34;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(686, 401);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(72, 25);
            this.label23.TabIndex = 33;
            this.label23.Text = "Insurer";
            // 
            // txtCDLno
            // 
            this.txtCDLno.Location = new System.Drawing.Point(305, 400);
            this.txtCDLno.Name = "txtCDLno";
            this.txtCDLno.Size = new System.Drawing.Size(292, 26);
            this.txtCDLno.TabIndex = 32;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(31, 400);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(166, 25);
            this.label22.TabIndex = 31;
            this.label22.Text = "Driver Licence No";
            // 
            // txtCIdNo
            // 
            this.txtCIdNo.Location = new System.Drawing.Point(928, 343);
            this.txtCIdNo.Name = "txtCIdNo";
            this.txtCIdNo.Size = new System.Drawing.Size(292, 26);
            this.txtCIdNo.TabIndex = 30;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(685, 346);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(148, 25);
            this.label21.TabIndex = 29;
            this.label21.Text = "Customer Id No";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(305, 345);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(292, 26);
            this.txtEmail.TabIndex = 28;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(31, 346);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(81, 25);
            this.label20.TabIndex = 27;
            this.label20.Text = "Email Id";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(929, 293);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(292, 26);
            this.txtPhone.TabIndex = 26;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(686, 296);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(138, 25);
            this.label19.TabIndex = 25;
            this.label19.Text = "Cell Phone No";
            // 
            // txtCBAdd
            // 
            this.txtCBAdd.Location = new System.Drawing.Point(305, 293);
            this.txtCBAdd.Name = "txtCBAdd";
            this.txtCBAdd.Size = new System.Drawing.Size(292, 26);
            this.txtCBAdd.TabIndex = 24;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(31, 294);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(260, 25);
            this.label18.TabIndex = 23;
            this.label18.Text = "Customer Business Address";
            // 
            // txtCHAdd
            // 
            this.txtCHAdd.Location = new System.Drawing.Point(929, 246);
            this.txtCHAdd.Name = "txtCHAdd";
            this.txtCHAdd.Size = new System.Drawing.Size(292, 26);
            this.txtCHAdd.TabIndex = 22;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(686, 248);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(232, 25);
            this.label17.TabIndex = 21;
            this.label17.Text = "Customer Home Address";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(305, 240);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(292, 26);
            this.txtName.TabIndex = 20;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(31, 240);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(154, 25);
            this.label16.TabIndex = 19;
            this.label16.Text = "Customer Name";
            // 
            // txtReadingin
            // 
            this.txtReadingin.Location = new System.Drawing.Point(929, 190);
            this.txtReadingin.Name = "txtReadingin";
            this.txtReadingin.Size = new System.Drawing.Size(292, 26);
            this.txtReadingin.TabIndex = 16;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(685, 192);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(141, 25);
            this.label15.TabIndex = 15;
            this.label15.Text = "KM Reading In";
            // 
            // txtReadingout
            // 
            this.txtReadingout.Location = new System.Drawing.Point(305, 189);
            this.txtReadingout.Name = "txtReadingout";
            this.txtReadingout.Size = new System.Drawing.Size(292, 26);
            this.txtReadingout.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(31, 189);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(157, 25);
            this.label14.TabIndex = 13;
            this.label14.Text = "KM Reading Out";
            // 
            // txtArd
            // 
            this.txtArd.Location = new System.Drawing.Point(305, 134);
            this.txtArd.Name = "txtArd";
            this.txtArd.Size = new System.Drawing.Size(292, 26);
            this.txtArd.TabIndex = 12;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(31, 133);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(175, 25);
            this.label13.TabIndex = 11;
            this.label13.Text = "Actual Return Date";
            // 
            // txtLoandtto
            // 
            this.txtLoandtto.Location = new System.Drawing.Point(929, 132);
            this.txtLoandtto.Name = "txtLoandtto";
            this.txtLoandtto.Size = new System.Drawing.Size(292, 26);
            this.txtLoandtto.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(836, 133);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 25);
            this.label12.TabIndex = 9;
            this.label12.Text = "To";
            // 
            // Loandtfrom
            // 
            this.Loandtfrom.Location = new System.Drawing.Point(929, 79);
            this.Loandtfrom.Name = "Loandtfrom";
            this.Loandtfrom.Size = new System.Drawing.Size(292, 26);
            this.Loandtfrom.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(836, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 25);
            this.label11.TabIndex = 7;
            this.label11.Text = "From";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(685, 81);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 25);
            this.label10.TabIndex = 6;
            this.label10.Text = "Loan Date";
            // 
            // txtVehicle
            // 
            this.txtVehicle.Location = new System.Drawing.Point(305, 80);
            this.txtVehicle.Name = "txtVehicle";
            this.txtVehicle.Size = new System.Drawing.Size(292, 26);
            this.txtVehicle.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(31, 81);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 25);
            this.label9.TabIndex = 4;
            this.label9.Text = "Vehicle Details";
            // 
            // txtReg
            // 
            this.txtReg.Location = new System.Drawing.Point(929, 23);
            this.txtReg.Name = "txtReg";
            this.txtReg.Size = new System.Drawing.Size(292, 26);
            this.txtReg.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(685, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(144, 25);
            this.label8.TabIndex = 2;
            this.label8.Text = "Registration No";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(305, 23);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(292, 26);
            this.txtBarcode.TabIndex = 1;
            this.txtBarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(31, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 25);
            this.label7.TabIndex = 0;
            this.label7.Text = "Barcode";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(31, 556);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(103, 25);
            this.label28.TabIndex = 47;
            this.label28.Text = "Engine No";
            // 
            // txtEng
            // 
            this.txtEng.Location = new System.Drawing.Point(305, 557);
            this.txtEng.Name = "txtEng";
            this.txtEng.Size = new System.Drawing.Size(292, 26);
            this.txtEng.TabIndex = 48;
            // 
            // txtVinn
            // 
            this.txtVinn.Location = new System.Drawing.Point(929, 558);
            this.txtVinn.Name = "txtVinn";
            this.txtVinn.Size = new System.Drawing.Size(292, 26);
            this.txtVinn.TabIndex = 50;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(690, 557);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(77, 25);
            this.label29.TabIndex = 49;
            this.label29.Text = "VinnNo";
            // 
            // Agreement_and_Indemnity_Form
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1308, 828);
            this.Controls.Add(this.panel2);
            this.Name = "Agreement_and_Indemnity_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtReg;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtReadingin;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtReadingout;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtArd;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtLoandtto;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker Loandtfrom;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtVehicle;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtInsurance;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtCDLno;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtCIdNo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtCBAdd;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtCHAdd;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtCOVD;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtPolicyno;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cmbReferred;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtVinn;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtEng;
        private System.Windows.Forms.Label label28;
    }
}