﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BaronsTokai
{
    public partial class Agreement_and_Indemnity_Form : Formsdsfds
    {
        SqlConnection con = new SqlConnection("Data Source=IBUYAS\\KHANAIK;Initial Catalog=BaronsTokai;Integrated Security=True;");
        SqlCommand cmd;
        public Agreement_and_Indemnity_Form()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      /*  private void btnAdd_Click(object sender, EventArgs e)
        {
            cmd = new SqlCommand("INSERT INTO CustomerAgreement VALUES (@Barcode,@RegistrationNo,@VehicleModel,@LoanDateFrom,@LoanDateTo,@ActualReturnDate,@KMReadingout,@KMReadingin,@CustomerName,@CustomerHomeAddress,@CustomerBusinessAddress,@CellPhoneNo,@EmailId,@CustomerIdNumber,@DriverLicenceNo,@Insurer,@PolicyNo,@CustomerOwnVehicleDetails,@Date,@ReferredBy)", con);

            SqlParameter[] param = new SqlParameter[20];
            param[0] = new SqlParameter("@Barcode", SqlDbType.VarChar, 50);
            param[0].Value = txtBarcode.Text;

            param[1] = new SqlParameter("@RegistrationNo", SqlDbType.VarChar, 50);
            param[1].Value = txtReg.Text;

            param[2] = new SqlParameter("@VehicleModel", SqlDbType.VarChar, 50);
            param[2].Value = txtVehicle.Text;

            param[3] = new SqlParameter("@LoanDateFrom", SqlDbType.Date);
            param[3].Value = Loandtfrom.Value;

            param[4] = new SqlParameter("@LoanDateTo", SqlDbType.Date);
            param[4].Value =txtLoandtto.Text;

            param[5] = new SqlParameter("@ActualReturnDate", SqlDbType.Date);
            param[5].Value = txtArd.Text;

            param[6] = new SqlParameter("@KMReadingout", SqlDbType.VarChar,10);
            param[6].Value = txtReadingout.Text;

            param[7] = new SqlParameter("@KMReadingin", SqlDbType.VarChar, 10);
            param[7].Value = txtReadingin.Text;

            param[8] = new SqlParameter("@CustomerName", SqlDbType.VarChar, 10);
            param[8].Value = txtName.Text;

            param[9] = new SqlParameter("@CustomerHomeAddress", SqlDbType.VarChar, 10);
            param[9].Value = txtCHAdd.Text;

            param[10] = new SqlParameter("@CustomerBusinessAddress", SqlDbType.VarChar, 10);
            param[10].Value = txtCBAdd.Text;

            param[11] = new SqlParameter("@CellPhoneNo", SqlDbType.VarChar, 10);
            param[11].Value = txtPhone.Text;

            param[12] = new SqlParameter("@EmailId", SqlDbType.VarChar, 10);
            param[12].Value = txtEmail.Text;

            param[13] = new SqlParameter("@CustomerIdNumber", SqlDbType.VarChar, 10);
            param[13].Value = txtCIdNo.Text;

            param[14] = new SqlParameter("@DriverLicenceNo", SqlDbType.VarChar, 10);
            param[14].Value = txtCDLno.Text;

            param[15] = new SqlParameter("@Insurer", SqlDbType.VarChar, 10);
            param[15].Value = txtInsurance.Text;

            param[16] = new SqlParameter("@PolicyNo", SqlDbType.VarChar, 10);
            param[16].Value = txtPolicyno.Text;

            param[17] = new SqlParameter("@CustomerOwnVehicleDetails", SqlDbType.VarChar, 10);
            param[17].Value = txtCOVD.Text;

            param[18] = new SqlParameter("@Date", SqlDbType.Date);
            param[18].Value = dtpDate.Value;

            param[19] = new SqlParameter("@ReferredBy", SqlDbType.VarChar, 10);
            param[19].Value = cmbReferred.Text;

            con.Open();
            cmd.Parameters.AddRange(param);
            cmd.ExecuteNonQuery();
            con.Close();
            MessageBox.Show("Records Added Successfully", "Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            ClearData();
        }*/

        private void txtBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    cmd = new SqlCommand("SELECT * FROM BarcodeGen WHERE Barcode=@Barcode", con);
                    cmd.Parameters.Add(new SqlParameter("@Barcode", txtBarcode.Text));
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        txtReg.Text = reader.GetString(2);
                        txtVehicle.Text = reader.GetString(3);
                        txtEng.Text = reader.GetString(4);
                        txtVinn.Text = reader.GetString(5);
                        
                    }
                    reader.Close();
                    con.Close();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void CustomerDetails(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CustomerDetails cd = new CustomerDetails();
            cd.Show();
            this.Hide();
        }

        private void ClearData()
        {
            txtBarcode.Text = "";
            txtReg.Text = "";
            txtVehicle.Text = "";
            txtReadingout.Text = "";
            txtReadingin.Text = "";
            txtName.Text = "";
            txtCHAdd.Text = "";
            txtCBAdd.Text = "";
            txtPhone.Text = "";
            txtEmail.Text = "";
            txtCIdNo.Text = "";
            txtCDLno.Text = "";
            txtInsurance.Text = "";
            txtPolicyno.Text = "";
            txtCOVD.Text = "";
            dtpDate.Text = "";
            cmbReferred.Text = "";
            txtArd.Text = "";
            txtLoandtto.Text = "";
            txtEng.Text = "";
            txtVinn.Text = "";

        } 

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void linkLabel1_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Form1 form = new Form1();
            form.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                cmd = new SqlCommand("INSERT INTO CustomerAgreement VALUES (@Barcode,@RegistrationNo,@VehicleModel,@LoanDateFrom,@LoanDateTo,@ActualReturnDate,@KMReadingout,@KMReadingin,@CustomerName,@CustomerHomeAddress,@CustomerBusinessAddress,@CellPhoneNo,@EmailId,@CustomerIdNumber,@DriverLicenceNo,@Insurer,@PolicyNo,@CustomerOwnVehicleDetails,@Date,@ReferredBy,@EngineNo,@VinnNo)", con);

                SqlParameter[] param = new SqlParameter[22];
                param[0] = new SqlParameter("@Barcode", SqlDbType.VarChar, 50);
                param[0].Value = txtBarcode.Text;

                param[1] = new SqlParameter("@RegistrationNo", SqlDbType.VarChar, 50);
                param[1].Value = txtReg.Text;

                param[2] = new SqlParameter("@VehicleModel", SqlDbType.VarChar, 50);
                param[2].Value = txtVehicle.Text;

                param[3] = new SqlParameter("@LoanDateFrom", SqlDbType.Date);
                param[3].Value = Loandtfrom.Value;

                param[4] = new SqlParameter("@LoanDateTo", SqlDbType.VarChar, 10);
                param[4].Value = txtLoandtto.Text;

                param[5] = new SqlParameter("@ActualReturnDate", SqlDbType.VarChar, 10);
                param[5].Value = txtArd.Text;

                param[6] = new SqlParameter("@KMReadingout", SqlDbType.VarChar, 10);
                param[6].Value = txtReadingout.Text;

                param[7] = new SqlParameter("@KMReadingin", SqlDbType.VarChar, 10);
                param[7].Value = txtReadingin.Text;

                param[8] = new SqlParameter("@CustomerName", SqlDbType.VarChar, 10);
                param[8].Value = txtName.Text;

                param[9] = new SqlParameter("@CustomerHomeAddress", SqlDbType.VarChar, 10);
                param[9].Value = txtCHAdd.Text;

                param[10] = new SqlParameter("@CustomerBusinessAddress", SqlDbType.VarChar, 10);
                param[10].Value = txtCBAdd.Text;

                param[11] = new SqlParameter("@CellPhoneNo", SqlDbType.VarChar, 10);
                param[11].Value = txtPhone.Text;

                param[12] = new SqlParameter("@EmailId", SqlDbType.VarChar, 10);
                param[12].Value = txtEmail.Text;

                param[13] = new SqlParameter("@CustomerIdNumber", SqlDbType.VarChar, 10);
                param[13].Value = txtCIdNo.Text;

                param[14] = new SqlParameter("@DriverLicenceNo", SqlDbType.VarChar, 10);
                param[14].Value = txtCDLno.Text;

                param[15] = new SqlParameter("@Insurer", SqlDbType.VarChar, 10);
                param[15].Value = txtInsurance.Text;

                param[16] = new SqlParameter("@PolicyNo", SqlDbType.VarChar, 10);
                param[16].Value = txtPolicyno.Text;

                param[17] = new SqlParameter("@CustomerOwnVehicleDetails", SqlDbType.VarChar, 10);
                param[17].Value = txtCOVD.Text;

                param[18] = new SqlParameter("@Date", SqlDbType.Date);
                param[18].Value = dtpDate.Value;

                param[19] = new SqlParameter("@ReferredBy", SqlDbType.VarChar, 10);
                param[19].Value = cmbReferred.Text;

                param[20] = new SqlParameter("@EngineNo", SqlDbType.VarChar, 50);
                param[20].Value = txtEng.Text;

                param[21] = new SqlParameter("@VinnNo", SqlDbType.VarChar, 50);
                param[21].Value = txtVinn.Text;


                con.Open();
                cmd.Parameters.AddRange(param);
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Records Added Successfully", "Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearData();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
