﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaronsTokai
{
    public partial class CustomerDetails : Form
    {
        SqlConnection con = new SqlConnection("Data Source=IBUYAS\\KHANAIK;Initial Catalog=BaronsTokai;Integrated Security=True;");
        SqlCommand cmd;

        int SNo = 0;

        public CustomerDetails()
        {
            InitializeComponent();
           
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(txtName.Text!="" && txtCHAdd.Text!="" && txtCBAdd.Text!="" && txtPhone.Text!="" && txtEmail.Text!="" && txtCIdNo.Text!="" && txtCDLno.Text!="" && txtInsurance.Text!="" && txtPolicyno.Text!="" && txtCOVD.Text!="" && dtpDate.Text!="" && cmbReffered.Text!="")
            {
                cmd = new SqlCommand("Insert into CustomerDetails (CustomerName,CustomerHomeAddress,CustomerBusinessAddress,CellPhoneNo,EmailId,CustomerIdNumber,DriverLicenceNo,Insurer,PolicyNo,CustomerOwnVehicleDetails,Date,ReferredBy) values (@CustomerName,@CustomerHomeAddress,@CustomerBusinessAddress,@CellPhoneNo,@EmailId,@CustomerIdNumber,@DriverLicenceNo,@Insurer,@PolicyNo,@CustomerOwnVehicleDetails,@Date,@ReferredBy)", con);
                con.Open();
                cmd.Parameters.AddWithValue("@CustomerName", txtName.Text);
                cmd.Parameters.AddWithValue("@CustomerHomeAddress", txtCHAdd.Text);
                cmd.Parameters.AddWithValue("@CustomerBusinessAddress", txtCBAdd.Text);
                cmd.Parameters.AddWithValue("@CellPhoneNo", txtPhone.Text);
                cmd.Parameters.AddWithValue("@EmailId", txtEmail.Text);
                cmd.Parameters.AddWithValue("@CustomerIdNumber", txtCIdNo.Text);
                cmd.Parameters.AddWithValue("@DriverLicenceNo", txtCDLno.Text);
                cmd.Parameters.AddWithValue("@Insurer", txtInsurance.Text);
                cmd.Parameters.AddWithValue("@PolicyNo", txtPolicyno.Text);
                cmd.Parameters.AddWithValue("@CustomerOwnVehicleDetails", txtCOVD.Text);
                cmd.Parameters.AddWithValue("@Date", dtpDate.Value);
                cmd.Parameters.AddWithValue("@ReferredBy", cmbReffered.Text);
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Record Submitted Successfully");                
                ClearData();
            }
        }
        private void ClearData()
        {
            txtName.Text = "";
            txtCHAdd.Text = "";
            txtCBAdd.Text = "";
            txtPhone.Text = "";
            txtEmail.Text = "";
            txtCIdNo.Text = "";
            txtCDLno.Text = "";
            txtInsurance.Text = "";
            txtPolicyno.Text = "";
            txtCOVD.Text = "";
            dtpDate.Text = "";
            cmbReffered.Text = "";
            SNo = 0;
        }
    }
}
