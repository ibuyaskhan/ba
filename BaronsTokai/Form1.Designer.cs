﻿namespace BaronsTokai
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnEmployee = new System.Windows.Forms.Button();
            this.btnAgreement = new System.Windows.Forms.Button();
            this.btnBarcode = new System.Windows.Forms.Button();
            this.btnReport = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(644, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(408, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to Barons Tokai";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnEmployee
            // 
            this.btnEmployee.BackColor = System.Drawing.SystemColors.Menu;
            this.btnEmployee.Location = new System.Drawing.Point(748, 124);
            this.btnEmployee.Name = "btnEmployee";
            this.btnEmployee.Size = new System.Drawing.Size(157, 47);
            this.btnEmployee.TabIndex = 1;
            this.btnEmployee.Text = "Employee Update";
            this.btnEmployee.UseVisualStyleBackColor = false;
            // 
            // btnAgreement
            // 
            this.btnAgreement.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAgreement.Location = new System.Drawing.Point(603, 226);
            this.btnAgreement.Name = "btnAgreement";
            this.btnAgreement.Size = new System.Drawing.Size(472, 59);
            this.btnAgreement.TabIndex = 2;
            this.btnAgreement.Text = "Agreement and Indemnity : Motor Vehicle Loan / Courtesy Vehicle Form";
            this.btnAgreement.UseVisualStyleBackColor = false;
            this.btnAgreement.Click += new System.EventHandler(this.btnAgreement_Click);
            // 
            // btnBarcode
            // 
            this.btnBarcode.BackColor = System.Drawing.SystemColors.Menu;
            this.btnBarcode.Location = new System.Drawing.Point(730, 338);
            this.btnBarcode.Name = "btnBarcode";
            this.btnBarcode.Size = new System.Drawing.Size(205, 61);
            this.btnBarcode.TabIndex = 3;
            this.btnBarcode.Text = "Barcode Generator for Vehicle";
            this.btnBarcode.UseVisualStyleBackColor = false;
            this.btnBarcode.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // btnReport
            // 
            this.btnReport.BackColor = System.Drawing.SystemColors.Menu;
            this.btnReport.Location = new System.Drawing.Point(747, 459);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(175, 49);
            this.btnReport.TabIndex = 4;
            this.btnReport.Text = "Report Generation";
            this.btnReport.UseVisualStyleBackColor = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(748, 561);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(174, 42);
            this.btnUpdate.TabIndex = 5;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(1499, 665);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnReport);
            this.Controls.Add(this.btnBarcode);
            this.Controls.Add(this.btnAgreement);
            this.Controls.Add(this.btnEmployee);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Main Screen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEmployee;
        private System.Windows.Forms.Button btnAgreement;
        private System.Windows.Forms.Button btnBarcode;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Button btnUpdate;
    }
}

