﻿namespace BaronsTokai
{
    partial class CustomerDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtCHAdd = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCBAdd = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCIdNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCDLno = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtInsurance = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPolicyno = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCOVD = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbReffered = new System.Windows.Forms.ComboBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CName
            // 
            this.CName.AutoSize = true;
            this.CName.Location = new System.Drawing.Point(26, 40);
            this.CName.Name = "CName";
            this.CName.Size = new System.Drawing.Size(124, 20);
            this.CName.TabIndex = 0;
            this.CName.Text = "Customer Name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(242, 40);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(291, 26);
            this.txtName.TabIndex = 1;
            // 
            // txtCHAdd
            // 
            this.txtCHAdd.Location = new System.Drawing.Point(807, 37);
            this.txtCHAdd.Name = "txtCHAdd";
            this.txtCHAdd.Size = new System.Drawing.Size(291, 26);
            this.txtCHAdd.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(575, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(188, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Customer Home Address";
            // 
            // txtCBAdd
            // 
            this.txtCBAdd.Location = new System.Drawing.Point(242, 96);
            this.txtCBAdd.Name = "txtCBAdd";
            this.txtCBAdd.Size = new System.Drawing.Size(291, 26);
            this.txtCBAdd.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Customer Business Address";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(807, 90);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(291, 26);
            this.txtPhone.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(575, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Cell Phone No";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(242, 154);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(291, 26);
            this.txtEmail.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Email Id";
            // 
            // txtCIdNo
            // 
            this.txtCIdNo.Location = new System.Drawing.Point(807, 148);
            this.txtCIdNo.Name = "txtCIdNo";
            this.txtCIdNo.Size = new System.Drawing.Size(291, 26);
            this.txtCIdNo.TabIndex = 11;
            this.txtCIdNo.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(575, 157);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "Customer Id Number";
            // 
            // txtCDLno
            // 
            this.txtCDLno.Location = new System.Drawing.Point(242, 216);
            this.txtCDLno.Name = "txtCDLno";
            this.txtCDLno.Size = new System.Drawing.Size(291, 26);
            this.txtCDLno.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 216);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(169, 20);
            this.label7.TabIndex = 12;
            this.label7.Text = "Driver Licence Number";
            // 
            // txtInsurance
            // 
            this.txtInsurance.Location = new System.Drawing.Point(807, 210);
            this.txtInsurance.Name = "txtInsurance";
            this.txtInsurance.Size = new System.Drawing.Size(291, 26);
            this.txtInsurance.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(575, 219);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(160, 20);
            this.label8.TabIndex = 14;
            this.label8.Text = "Insurer (If Applicable)";
            // 
            // txtPolicyno
            // 
            this.txtPolicyno.Location = new System.Drawing.Point(242, 273);
            this.txtPolicyno.Name = "txtPolicyno";
            this.txtPolicyno.Size = new System.Drawing.Size(291, 26);
            this.txtPolicyno.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(26, 273);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 20);
            this.label9.TabIndex = 16;
            this.label9.Text = "Policy No";
            // 
            // txtCOVD
            // 
            this.txtCOVD.Location = new System.Drawing.Point(807, 267);
            this.txtCOVD.Name = "txtCOVD";
            this.txtCOVD.Size = new System.Drawing.Size(291, 26);
            this.txtCOVD.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(575, 273);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(223, 20);
            this.label10.TabIndex = 18;
            this.label10.Text = "Customer Own Vehicle Details";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(26, 328);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 20);
            this.label11.TabIndex = 20;
            this.label11.Text = "Date";
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(236, 328);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(297, 26);
            this.dtpDate.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(575, 328);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 20);
            this.label12.TabIndex = 22;
            this.label12.Text = "Reffered By";
            // 
            // cmbReffered
            // 
            this.cmbReffered.FormattingEnabled = true;
            this.cmbReffered.Items.AddRange(new object[] {
            "Riedwan",
            "Basheer",
            "Ibuyas",
            "Salman",
            "Khan"});
            this.cmbReffered.Location = new System.Drawing.Point(807, 325);
            this.cmbReffered.Name = "cmbReffered";
            this.cmbReffered.Size = new System.Drawing.Size(291, 28);
            this.cmbReffered.TabIndex = 23;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(30, 389);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(107, 44);
            this.btnAdd.TabIndex = 24;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // CustomerDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 636);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.cmbReffered);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtCOVD);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtPolicyno);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtInsurance);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtCDLno);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtCIdNo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCBAdd);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCHAdd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.CName);
            this.Name = "CustomerDetails";
            this.Text = "CustomerDetails";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtCHAdd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCBAdd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCIdNo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCDLno;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtInsurance;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPolicyno;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCOVD;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbReffered;
        private System.Windows.Forms.Button btnAdd;
    }
}