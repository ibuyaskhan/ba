﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaronsTokai
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnAgreement_Click(object sender, EventArgs e)
        {
            Agreement_and_Indemnity_Form agree = new Agreement_and_Indemnity_Form();
            agree.Show();
            this.Hide();
        }

        private void btnBarcode_Click(object sender, EventArgs e)
        {
            BarcodeGen bgn = new BarcodeGen();
            bgn.Show();
            this.Hide();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateForm uf = new UpdateForm();
            uf.Show();
            this.Hide();
        }
    }
}
