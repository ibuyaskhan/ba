﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaronsTokai
{
    public partial class BarcodeGen : Form
    {
        SqlConnection con = new SqlConnection("Data Source=IBUYAS\\KHANAIK;Initial Catalog=BaronsTokai;Integrated Security=True;");
        SqlCommand cmd;

        public BarcodeGen()
        {
            InitializeComponent();
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            try
            {
                cmd = new SqlCommand("INSERT INTO BarcodeGen VALUES (@Barcode,@RegistrationNo,@VehicleModel,@EngineNo,@VinnNo)", con);

                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@Barcode", SqlDbType.VarChar, 50);
                param[0].Value = txtBarcode.Text;

                param[1] = new SqlParameter("@RegistrationNo", SqlDbType.VarChar, 50);
                param[1].Value = txtReg.Text;

                param[2] = new SqlParameter("@VehicleModel", SqlDbType.VarChar, 50);
                param[2].Value = txtVehicle.Text;

                param[3] = new SqlParameter("@EngineNo", SqlDbType.VarChar, 50);
                param[3].Value = txtEng.Text;

                param[4] = new SqlParameter("@VinnNo", SqlDbType.VarChar, 50);
                param[4].Value = txtVinn.Text;

                con.Open();
                cmd.Parameters.AddRange(param);
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Records Added Successfully", "Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearData();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void ClearData()
        {
            txtBarcode.Text = "";
            txtReg.Text = "";
            txtVehicle.Text = "";
            txtEng.Text = "";
            txtVinn.Text = "";
        }

        private void linkLabel1_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            this.Hide();
        }
    }
}
