create Database BaronsTokai

use baronstokai

create table CustomerAgreement(SNo int primary key identity(1,1), Barcode varchar(50), RegistrationNo varchar(50), 
VehicleModel varchar(50), LoanDateFrom date, LoanDateTo date, ActualReturnDate date, KMReadingout varchar(10),
KMReadingin varchar(10),CustomerName varchar(50), CustomerHomeAddress varchar(200),
CustomerBusinessAddress varchar(200), CellPhoneNo varchar(10), EmailId varchar(100), CustomerIdNumber varchar(16), 
DriverLicenceNo varchar(16), Insurer varchar(100), PolicyNo varchar(50), CustomerOwnVehicleDetails varchar(100), 
Date date, ReferredBy varchar(50),EngineNo varchar(50), VinnNo varchar(50))

select * from CustomerAgreement

select * from CustomerAgreement where Barcode='123456789'

alter table Agreement alter column KMReadingout varchar(10)
alter table Agreement alter column KMReadingin varchar(10)

drop table CustomerAgreement

create table BarcodeGen(SNo int primary key identity(1,1), Barcode varchar(50), RegistrationNo varchar(50), 
VehicleModel varchar(50), EngineNo varchar(50), VinnNo varchar(50))

select * from BarcodeGen

drop table BarcodeGen

create table CustomerDetails(SNo int primary key identity(1,1), CustomerName varchar(50), CustomerHomeAddress varchar(200),
CustomerBusinessAddress varchar(200), CellPhoneNo varchar(10), EmailId varchar(100), CustomerIdNumber varchar(16), 
DriverLicenceNo varchar(16), Insurer varchar(100), PolicyNo varchar(50), CustomerOwnVehicleDetails varchar(100), 
Date date, ReferredBy varchar(50))

select * from Agreement

sp_help 'Agreement'

drop table CustomerDetails

select * from CustomerDetails

